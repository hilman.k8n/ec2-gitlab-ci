## Prerequisite
Make sure to set these environment variables in Settings -> CI/CD -> Variables
- ```AWS_ACCESS_KEY_ID```

  AWS access key ID to acces AWS API
- ```AWS_SECRET_ACCESS_KEY```

  AWS secret key access to acces AWS API
- ```AWS_DEFAULT_REGION```
  
  AWS region where this pipeline will run

- ```DOCKER_USERNAME```

  docker registry username
- ```DOCKER_PASSWORD```

  docker registry password

- Already run terraform apply on this [repo](https://gitlab.com/hilmandroid/ec2-vpc) and then update all the variables on .gitlab-ci.yml like
  - ```EC2_LAUNCH_TEMPLATE_SECURITY_GROUP_IDS```
  - ```EC2_PROVISION_SUBNET_ID```

## Pipeline

- build

  Every code commited to a branch (except master), will trigger these stages
  - build-docker

    build docker image based on nginx:1.18.0-alpine image with root /var/www 
  - build-packer

    build AWS AMI with docker daemon installed  and container running with image that was created in build-docker stage
  - launch-template

    create AWS EC2 Launch Template from AMI that was created in build-packer stage
- deploy

  Every code merged to master, will update autoscaling group to use new launch template and re-create all instance in autoscaling group with new launch template that was created in launch-template stage
  